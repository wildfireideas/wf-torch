<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wf-torch' );

/** MySQL database username */
define( 'DB_USER', 'wildfire' );

/** MySQL database password */
define( 'DB_PASSWORD', 'wildfire' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '2#t*](hz*0vD!fkfo1nz}?=CO=/BAr,BB^5vi042UscGOcT1e]jqC52G<~C9$sV+' );
define( 'SECURE_AUTH_KEY',  'fnQ73&#wQa^:kaLV_gsV.qELc&^84spM{t_iTM$Z Fo(.s5y(Gs~`?1Ge`D9x{8v' );
define( 'LOGGED_IN_KEY',    'F9SiRjKa+Q@4WZaFT##R#`@.70vY~Y.oK(68*ZxjK1F)2`=/P7{=z0etjP]37]F}' );
define( 'NONCE_KEY',        '`)#SnhnWIwt/Cw!Yo&r%DJEp6WdJkO.c`7hX98S_SsXti;tg^gX9R9|ZhC-i}g16' );
define( 'AUTH_SALT',        '[orbi<V1GnKl=MA~8ttS(j/n9BNQX{1dEn|zA|E2TDZ_X*`1{,r+zqt,hp&97hVV' );
define( 'SECURE_AUTH_SALT', 'c>`N;ir1<F^=h:.xGvLJO+y3tj^5D)Y*hcRr$9Hd )_.L?l_]Y3D<q@$QIzWz?gG' );
define( 'LOGGED_IN_SALT',   'TO|Jpq`h^wYVfp_)J&lrm;h)8nM9#~3/i?H*LBE]G-?8|t|dMjsfu=gQ~iZ%y]`I' );
define( 'NONCE_SALT',       'uBvx~(dpWuBjz|ZJ9UGDCWTWIWq)d8k&832(_dIh34,4+xwSx1*`vw)NwH+HEKnq' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wf_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
